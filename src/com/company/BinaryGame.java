package com.company;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.math.BigInteger;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
public class BinaryGame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Screen screen;
	private int length;
	private static final int DEFAULT_LENGTH = 7;
	private String current;
	private String target;
	private String start;
	private int movesUsed;
	private int minMoves;
	private static final int WIDTH = 600;
	private static final int HEIGHT = 600;
	private static BinaryGame theInstance = new BinaryGame();
	public static BinaryGame getInstance () {
		return theInstance;
	}
	private BinaryGame() {
		this.length = DEFAULT_LENGTH;
	}
	private void initialize() {
		this.length = DEFAULT_LENGTH;
		this.generatePuzzle(length);
		this.screen = new Screen();
		this.add(screen);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(WIDTH, HEIGHT);
		
	}
	public int getMinMoves(){
		return minMoves;
	}
	public void generatePuzzle(int length) {
		this.setTarget(generateRandomString(length));
		this.setStart(generateRandomString(length));
		this.setCurrent(start);
		this.minMoves = findMin(start, target);
		/*if(screen!=null) {
			this.remove(screen);
			this.screen = new Screen();
			this.add(screen);
		}*/
		this.repaint();
	}
	public String generateRandomString(int length) {
		String result = "";
		for(int i = 0 ; i < length ; ++i) {
			result += (int) (Math.random()*2);
		}
		return result;
	}
	private void setStart(String start) {
		this.start = start;
	}
	@Override
	public void repaint(){
		if(screen != null) {
			this.screen.paintComponents(this.getGraphics());
			this.screen.updateTextFields();
		}
	}
	public static void main(String args[]) {
		getInstance().initialize();
		getInstance().repaint();
		JOptionPane.showMessageDialog(BinaryGame.getInstance(), "Get to the target in the minimum number of moves.\n Click two bits to make a selection. \n Press \"Flip\" to make a move that will flip all selected bits. \n Click the first clicked bit again to de-select ", "Game Start", JOptionPane.INFORMATION_MESSAGE);
	}
	public int getMovesUsed() {
		return movesUsed;
	}
	public void setMovesUsed(int movesUsed) {
		this.movesUsed = movesUsed;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public String getCurrent() {
		return current;
	}
	public void setCurrent(String current) {
		this.current = current;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	/**
	 * @param start
	 * @param end
	 * @param toFlip
	 * @return a string that has flipped the bits from start to end inclusive in toFlip
	 */
	public String flip(int start, int end, String toFlip) {
		char[] flip = toFlip.toCharArray();
		for (int i = start; i <= end; ++i) {
			if(flip[i] == '1') {
				flip[i] = '0';
			}
			else {
				flip[i] = '1';
			}
			
		}
		return new String(flip);
	}
	public void reset() {
		movesUsed = 0;
		current = start;
	}
	/**
	 * Does the move, will set a flag if the game ends.
	 * @param start
	 * @param end
	 * @return true if the move was valid
	 */
	public boolean doMove(int start, int end) {
		if(start != end && start >= 0 && end >= 0 && start < length && end < length) {
			if(end > start) {
				this.current = flip(start,end,current);
			}
			else {
				this.current = flip(end,start,current);
			}
			++this.movesUsed;
			return true;
		}
		return false;
	}
	public int findMin(String currentString, String targetString) {
		BigInteger current = new BigInteger(currentString,2);
		BigInteger target = new BigInteger(targetString,2);
		BigInteger difference = current.xor(target);
		BigInteger permuted = difference.shiftLeft(1).xor(difference);
		
		int result = permuted.bitCount() / 2;
		if(difference.bitCount() == 1) {
			return 2;
		}
		return result;
	}
}
