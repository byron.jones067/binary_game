package com.company;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.math.BigInteger;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Screen extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5994640341023669318L;
	private ScreenBits screenBits;
	private JTextField currentDisplay;
	private JTextField targetDisplay;
	private JTextField movesMade;
	public static final int BITS_HEIGHT = 200;
	public int getScreenWidth(){
		return this.getWidth();
	}
	public int getScreenHeight(){
		return this.getHeight();
	}
	public Screen() {
		screenBits = new ScreenBits(BinaryGame.getInstance().getLength());
		screenBits.paintBits();
		this.setLayout(new GridLayout(2, 1));
		this.add(screenBits);
		this.setSize(BinaryGame.getInstance().getSize());
		this.add(addMenu());
	}
	private JPanel addMenu() {
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(5,1));
		currentDisplay = new JTextField("Current: " + new BigInteger(BinaryGame.getInstance().getCurrent(),2));
		currentDisplay.setEditable(false);
		targetDisplay = new JTextField("Target: " + new BigInteger(BinaryGame.getInstance().getTarget(),2));
		targetDisplay.setEditable(false);
		JButton flipper = new JButton("Flip");
		JButton newButton = new JButton("New");
		//JButton instructionButton  = new JButton()
		movesMade = new JTextField("Moves made: " + BinaryGame.getInstance().getMovesUsed());
		movesMade.setEditable(false);
		flipper.addActionListener(new FlipListener());
		newButton.addActionListener(new FlipListener());
		panel.add(flipper);
		panel.add(newButton);
		panel.add(currentDisplay);
		panel.add(targetDisplay);
		panel.add(movesMade);
		return panel;
	}
	@Override
	public void paintComponents(Graphics g) {
		this.updateTextFields();
		this.screenBits.paintBits();
	}
	public void updateTextFields() {
		currentDisplay.setText("Current: " + new BigInteger(BinaryGame.getInstance().getCurrent(),2));
		targetDisplay.setText("Target: " + new BigInteger(BinaryGame.getInstance().getTarget(),2));
		movesMade.setText("Moves made: " + BinaryGame.getInstance().getMovesUsed());
	
	}
	public void reset(){
		screenBits.reLength(BinaryGame.getInstance().getLength());
		this.screenBits.setVisible(true);
	}
	private class FlipListener implements ActionListener {
		
		FlipListener() {
			
		}
		@Override
		public void actionPerformed(ActionEvent event) {
			if (event.getActionCommand() == "Flip") {
				boolean success = screenBits.doFlip();
				if(!success) { 
					JOptionPane.showMessageDialog(BinaryGame.getInstance(), "You have not made a selection!", "Congratulations", JOptionPane.INFORMATION_MESSAGE);
					}
				else {
					updateTextFields();
				}
				
			}
			else if (event.getActionCommand() == "New") {
				new NewGameDialog();
			}
		}
	}
	
	private class ScreenBits extends JPanel {
		private ArrayList<ScreenBit> components;
		private int lastClickedIndex = -1;
		private int firstClickedIndex = -1;
		public ScreenBits(int length) {
			components = new ArrayList<ScreenBit>();
			this.setLayout(new GridLayout(1,length));
			for (int i = 0; i < length; ++i) {
				components.add(new ScreenBit(i));
				this.add(components.get(i));
			}
			this.setPreferredSize(new Dimension(getScreenWidth(),BITS_HEIGHT));
		}
		public void reLength(int length) {
			for(ScreenBit c : components){
				this.remove(c);
			}
			components = new ArrayList<ScreenBit>();
			this.setLayout(new GridLayout(1,length));
			for (int i = 0; i < length; ++i) {
				components.add(new ScreenBit(i));
				this.add(components.get(i));
			}
			this.repaint();
		}
		private void reset() {
			for (ScreenBit bit : components) {
				bit.setSelected(false);
				
			}
		}
		private void processSelection(int index) {
			if(firstClickedIndex == -1){
				components.get(index).setFirst(true);
				components.get(index).setSelected(true);
				firstClickedIndex = index;
				return;
			}
			if(firstClickedIndex == index) {
				for(ScreenBit c : components) {
					c.setSelected(false);
					c.setFirst(false);
					c.repaint();
				}
				firstClickedIndex = -1;
				lastClickedIndex = -1;
			}
			else {
				lastClickedIndex = index;
				int start = Math.min(firstClickedIndex, lastClickedIndex);
				int end = Math.max(firstClickedIndex, lastClickedIndex);
				for(int i = 0; i < components.size(); ++i) {
					ScreenBit curr = components.get(i);
					curr.setSelected(i >= start && i <= end);
					curr.setFirst(i == firstClickedIndex);
				}
				this.repaint();
			}
		}
		public boolean doFlip() {
			if(this.lastClickedIndex != -1 && this.firstClickedIndex != -1) {
				int start = Math.min(firstClickedIndex, lastClickedIndex);
				int end = Math.max(firstClickedIndex, lastClickedIndex);
				BinaryGame.getInstance().doMove(start, end);
				this.firstClickedIndex = -1;
				this.lastClickedIndex = -1;
				for(ScreenBit b : components){
					b.setFirst(false);
					b.setSelected(false);
				}
				paintBits();
				if(BinaryGame.getInstance().getCurrent().equals(BinaryGame.getInstance().getTarget())){
					JOptionPane.showMessageDialog(BinaryGame.getInstance(), "You won the game in " + BinaryGame.getInstance().getMovesUsed() + " moves\nThe minimum number of moves was " + BinaryGame.getInstance().getMinMoves(), "Error", JOptionPane.INFORMATION_MESSAGE);
				}
				return true;
			}
			return false;
		}
		public void paintBits() {
			String current = BinaryGame.getInstance().getCurrent();
			for(int i = 0 ; i < components.size() ; ++i) {
				components.get(i).setValue(current.charAt(i)=='1');
			}
			repaint();
			
		}
		private class ScreenBit extends JPanel{
			boolean selected = false;
			boolean value = true;
			boolean first = false;
			private final int index;
			public ScreenBit(int index) {
				this.index = index;
				this.addMouseListener(new BitFlipListener());
			}
			public void setValue(boolean value) {
				this.value = value; 
			}
			public void setFirst(boolean first) {
				this.first = first;
			}
			public class BitFlipListener implements MouseListener {
				@Override
				public void mouseClicked(MouseEvent event) {
					processSelection(index);
					repaint();
				}
				@Override
				public void mouseEntered(MouseEvent event) {}
				@Override
				public void mouseExited(MouseEvent event) {}
				@Override
				public void mousePressed(MouseEvent event) {}
				@Override
				public void mouseReleased(MouseEvent event) {}
			}
			public void setSelected(boolean selected) {
				this.selected = selected;
			}
			public void paintComponent(Graphics g) {
				if(g== null) {
					return;
				}
				g.drawRect(0, 0, this.getWidth()-1, this.getHeight()-1);
				highlight(g);
				g.setColor(TEXT_COLOR);
				if(this.value){
					drawOne(g);
				}
				else {
					drawZero(g);
				}
			}
			public void highlight(Graphics g) {
				g.setColor(getBackgroundColor());
				g.fillRect(0,0,this.getWidth()-1, this.getHeight()-1);
			}
			public void drawOne(Graphics g){
				g.setColor(TEXT_COLOR);
				g.fillRect((this.getWidth() * 7)/16,this.getHeight()/8,this.getWidth()/8,this.getHeight()/4 * 3);
			}
			public void drawZero(Graphics g){
				g.setColor(TEXT_COLOR);
				g.fillOval(this.getWidth()/8, this.getHeight()/8 ,this.getWidth()/4 * 3,this.getHeight()/4 * 3);
				g.setColor(getBackgroundColor());
				g.fillOval(this.getWidth()/4, this.getHeight()/4 ,this.getWidth()/2,this.getHeight()/2);
			}
			public Color getBackgroundColor(){
				return selected?(first?FIRST_COLOR:HIGHLIGHT):UNSELECTED;
			}
		}
	}
	public static final Color HIGHLIGHT = Color.BLUE;
	public static final Color UNSELECTED = Color.LIGHT_GRAY;
	public static final Color TEXT_COLOR = Color.ORANGE;
	public static final Color FIRST_COLOR = Color.PINK;
}
