package com.company;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class LogicTests {
	private static BinaryGame game;
	@BeforeClass
	public static void testSetUp() {
		game = BinaryGame.getInstance();
	}
	@org.junit.Test
	public void testFlip() {
		assertEquals("10111", game.flip(0, 2, "01011"));
		assertEquals("10101", game.flip(0, 3, "01011"));
		assertEquals("0000000", game.flip(0, 6, "1111111"));
		assertEquals("10101010", game.flip(0, 7, "01010101"));
	}
	@Test
	public void testMinValue() {
		assertEquals(2, game.findMin("1000", "1001"));
		assertEquals(3, game.findMin("00000", "10101"));
		assertEquals(1, game.findMin("0001111", "1110000"));
	}
	@Test
	public void testDoMove() {
		game.setLength(6);
		game.setCurrent("100010");
		//allowed
		assertTrue(game.doMove(0, 2));
		assertTrue(game.doMove(4, 3));
		//single bit, not allowed
		assertFalse(game.doMove(4, 4));
		//out of bounds
		assertFalse(game.doMove(-1, 7));
		assertFalse(game.doMove(-1,  3));
		assertFalse(game.doMove(4, 10));
	}
}
