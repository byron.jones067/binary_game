package com.company;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Dimension;

public class NewGameDialog extends JDialog {
	public static final Dimension PREF_SIZ=new Dimension(400,200);
	public NewGameDialog() {
		super(BinaryGame.getInstance(), ModalityType.APPLICATION_MODAL);
		JTextField length = new JTextField("Length: " + BinaryGame.getInstance().getLength());
		length.setEditable(true);
		length.setVisible(true);
		JButton cancel = new JButton("Cancel");
		JButton generate = new JButton("Generate");
		Action action = new Action();
		cancel.addActionListener(action);
		generate.addActionListener(action);
		
		
		this.setLayout(new GridLayout(1, 2));
		this.add(cancel);
		this.add(generate);
		this.setSize(PREF_SIZ);
		this.setVisible(true);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		//generate.addActionListener(action);
	}
	@Override
	public void dispose(){
		this.setVisible(false);
		BinaryGame.getInstance().repaint();
		super.dispose();
	}
	private class Action implements ActionListener {
		
		Action() {
			
		}
		@Override
		public void actionPerformed(ActionEvent event) {
			if (event.getActionCommand() == "Cancel") {
				dispose();
			}
			else if (event.getActionCommand() == "Generate") {
				/*
				 * create new puzzles
				 */
				BinaryGame.getInstance().generatePuzzle(BinaryGame.getInstance().getLength());
				BinaryGame.getInstance().reset();
				BinaryGame.getInstance().repaint();
				dispose();
			}
		}
	}
}
